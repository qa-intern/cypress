describe('Login Page Tests', () => {
    beforeEach(() => {
      // Load the fixture data before each test
      cy.fixture('loginData').as('loginData');
    });
  
    it('should successfully log in with valid credentials', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
     
      cy.get('input[type="email"]').type(this.loginData.validUser.email);
      cy.get('input[type="password"]').type(this.loginData.validUser.password);
      cy.get('button[type="submit"]').click();
      
  
      // Verify successful login by checking for the presence of a specific element visible after login
      cy.contains('Your Feed').should('be.visible');
    });
  
    it('should fail to log in with invalid credentials', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
      
      cy.get('input[type="email"]').type(this.loginData.invalidUser.email);
      cy.get('input[type="password"]').type(this.loginData.invalidUser.password);
      cy.get('button[type="submit"]').click();
     
      // Verify login failure by checking for an error message
     // cy.contains('email or password is invalid').should('be.visible');
    });
  
    it('should show an error message for empty email', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
      
      cy.get('input[type="password"]').type(this.loginData.validUser.password);
      cy.get('button[type="submit"]').click();
    
  
      // Verify login failure by checking for an error message
      //cy.get('.error-messages').should('contain', 'email can\'t be empty');
    });
  
    it('should show an error message for empty password', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
     
  
      cy.get('input[type="email"]').type(this.loginData.validUser.email);
      cy.get('button[type="submit"]').click();
    
      // Verify login failure by checking for an error message
      //cy.get('.error-messages').should('contain', 'passsword can\'t be empty');
    });
  
    it('should show an error message for an incorrect email format', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
    
  
      cy.get('input[type="email"]').type(this.loginData.invalidEmailFormat.email);
      cy.get('input[type="password"]').type(this.loginData.invalidEmailFormat.password);
      cy.get('button[type="submit"]').click();
      
  
      // Verify login failure by checking for an error message
     // cy.get('.error-messages').should('contain', 'email or password is invalid');
    });
  
    it('should show an error message for an incorrect password format', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
      
  
      cy.get('input[type="email"]').type(this.loginData.invalidPasswordFormat.email);
      cy.get('input[type="password"]').type(this.loginData.invalidPasswordFormat.password);
      cy.get('button[type="submit"]').click();
    
  
      // Verify login failure by checking for an error message
     // cy.get('.error-messages').should('contain', 'email or password is invalid');
    });
  
    it('should show an error message for an empty email field', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
      
      cy.get('input[type="password"]').type(this.loginData.emptyEmail.password);
      cy.get('button[type="submit"]').click();
     
      // Verify login failure by checking for an error message
     // cy.get('.error-messages').should('contain', 'email can\'t be empty');
    });
  
    it('should show an error message for an empty password field', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
     
      cy.get('input[type="email"]').type(this.loginData.emptyPassword.email);
      cy.get('button[type="submit"]').click();
     
      // Verify login failure by checking for an error message
      //cy.get('.error-messages').should('contain', 'password can\'t be empty');
    });
  
    it('should show an error message for an incorrect email', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
     
      cy.get('input[type="email"]').type(this.loginData.incorrectEmail.email);
      cy.get('input[type="password"]').type(this.loginData.incorrectEmail.password);
      cy.get('button[type="submit"]').click();
     
  
      // Verify login failure by checking for an error message
      //cy.get('.error-messages').should('contain', 'email or password is invalid');
    });
  
    it('should show an error message for an incorrect password', function() {
      cy.visit('https://next-realworld.vercel.app/');
      cy.contains('Sign in').click();
     
      cy.get('input[type="email"]').type(this.loginData.incorrectPassword.email);
      cy.get('input[type="password"]').type(this.loginData.incorrectPassword.password);
      cy.get('button[type="submit"]').click();
     
      // Verify login failure by checking for an error message
      //cy.get('.error-messages').should('contain', 'email or password is invalid');
    });
    it('should show an error message for both null email and password', function() {
        cy.visit('https://next-realworld.vercel.app/');
        cy.contains('Sign in').click();
       
    
      
      });

      });
    
  
  