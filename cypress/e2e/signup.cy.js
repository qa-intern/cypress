describe('signup', () => {
  
  it('Should allow a user to sign up with valid credentials', () => {
    cy.fixture('signupData').then((signupData) => {
      const user = signupData.validUser;
      cy.visit('https://next-realworld.vercel.app/user/register');
      cy.get('form').should('be.visible');
      cy.get(':nth-child(1) > .form-control').type(user.username); // valid credentials
      cy.get(':nth-child(2) > .form-control').type(user.email);
      cy.get(':nth-child(3) > .form-control').type(user.password);
      cy.get('.btn').click();
      // Add any additional assertions here to verify successful signup
    });
  });

  it('Should show validation errors for empty fields on signup', () => {
    cy.visit('https://next-realworld.vercel.app/user/register'); // empty fields
    cy.get('.btn').click();
    cy.get('.error-messages').should('contain', "can't be blank");
  });

  it('Should show error messages for duplicate email on signup', () => {
    cy.fixture('signupData').then((signupData) => {
      const user = signupData.duplicateEmail;
      cy.visit('https://next-realworld.vercel.app/user/register');
      cy.get(':nth-child(1) > .form-control').type(user.username);
      cy.get(':nth-child(2) > .form-control').type(user.email); // Using same email to test duplicate error
      cy.get(':nth-child(3) > .form-control').type(user.password);
      cy.get('.btn').click();
      // Add assertions here to verify duplicate email error message
    });
  });

  it('Should show error messages for invalid email on signup', () => {
    cy.fixture('signupData').then((signupData) => {
      const user = signupData.invalidEmail;
      cy.visit('https://next-realworld.vercel.app/user/register');
      cy.get(':nth-child(1) > .form-control').type(user.username);
      cy.get(':nth-child(2) > .form-control').type(user.email);
      cy.get(':nth-child(3) > .form-control').type(user.password);
      cy.get('.btn').click();
      // Add assertions here to verify invalid email error message
    });
  });
});
