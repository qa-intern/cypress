/// <reference types="cypress" />

describe('New Post Automation Testing', () => {
    beforeEach(() => {
      // Log in before each test
      cy.visit('https://next-realworld.vercel.app/user/login');
    cy.get('input[placeholder="Email"]').type('testingggggg12@gmail.com');
    cy.get('input[placeholder="Password"]').type('1234');
      cy.contains('button', 'Sign in').click();
      
      // Ensure login was successful
      cy.contains('Your Feed').should('be.visible');
    });
  
    it('should create a new post successfully', () => {
      // Visit the New Post page
      cy.visit('https://next-realworld.vercel.app/editor/new');
      
  
      // Fill out the form fields
      cy.get('input[placeholder="Article Title"]').type('My New Article');
     
      cy.get('input[placeholder="What\'s this article about?"]').type('This is a test article for Cypress automation.');
     
      cy.get('textarea[placeholder="Write your article (in markdown)"]').type('## This is a heading\n\nThis is some content for the test article.');
      
      cy.get('input[placeholder="Enter tags"]').type('test, cypress');
      
  
      // Click the 'Publish Article' button
      cy.contains('button', 'Publish Article').click();
      
  
      // Verify that the new post was created successfully
      cy.url().should('include', '/article/');
      
      cy.contains('My New Article').should('be.visible');

      cy.contains('## This is a heading').should('be.visible');
    });
  
    it('should show an error when the article title is empty', () => {
      // Visit the New Post page
      cy.visit('https://next-realworld.vercel.app/editor/new');
      cy.wait(5000); // wait for 5000ms
  
      // Fill out the form fields except for the article title
      cy.get('input[placeholder="What\'s this article about?"]').type('This article has no title.');
      
      cy.get('textarea[placeholder="Write your article (in markdown)"]').type('This is an article without a title.');
      
      cy.get('input[placeholder="Enter tags"]').type('no-title, cypress');
      
  
      // Click the 'Publish Article' button
      cy.contains('button', 'Publish Article').click();
      
      // Verify that an error message is displayed
      cy.contains('Article Title is required').should('be.visible');
    });
  
    it('should show an error when the tags are empty', () => {
        // Visit the New Post page
        cy.visit('https://next-realworld.vercel.app/editor/new');
        
    
        // Fill out the form fields except for the tags
        cy.get('input[placeholder="Article Title"]').type('My New Article');
        
        cy.get('input[placeholder="What\'s this article about?"]').type('This article has no title.');
        
        cy.get('textarea[placeholder="Write your article (in markdown)"]').type('This is an article without a title.');
        
       
        // Click the 'Publish Article' button
        cy.contains('button', 'Publish Article').click();
        
    
        // Verify that an error message is displayed
        cy.contains('tags are required').should('be.visible');
    });
  
    it('should show an error when "What\'s this article about?" is empty', () => {
        // Visit the New Post page
        cy.visit('https://next-realworld.vercel.app/editor/new');
       
    
        // Fill out the form fields except for the "What's this article about?" field
        cy.get('input[placeholder="Article Title"]').type('My New Article');
       
        cy.get('textarea[placeholder="Write your article (in markdown)"]').type('This is an article without a title.');
      
        cy.get('input[placeholder="Enter tags"]').type('no-title, cypress');
       
       
        // Click the 'Publish Article' button
        cy.contains('button', 'Publish Article').click();
      
    
        // Verify that an error message is displayed
        cy.contains('what is article about required').should('be.visible');
    });
  
    it('should show an error when "Write your article (in markdown)" is empty', () => {
        // Visit the New Post page
        cy.visit('https://next-realworld.vercel.app/editor/new');
        
    
        // Fill out the form fields except for the "Write your article (in markdown)" field
        cy.get('input[placeholder="Article Title"]').type('My New Article');
        
        cy.get('input[placeholder="What\'s this article about?"]').type('This article has no title.');
        
        cy.get('input[placeholder="Enter tags"]').type('no-title, cypress');
        
       
        // Click the 'Publish Article' button
        cy.contains('button', 'Publish Article').click();
       
        // Verify that an error message is displayed
        cy.contains('what is the article about in markdown must be required').should('be.visible');
    });
  
    it('should show an error when the article title and "What\'s this article about?" are empty', () => {
        // Visit the New Post page
        cy.visit('https://next-realworld.vercel.app/editor/new');
        
    
        // Fill out the form fields except for the article title and "What's this article about?" fields
        cy.get('textarea[placeholder="Write your article (in markdown)"]').type('This is an article without a title.');
       
        cy.get('input[placeholder="Enter tags"]').type('no-title, cypress');
        
    
        // Click the 'Publish Article' button
        cy.contains('button', 'Publish Article').click();
       
        // Verify that an error message is displayed
        cy.contains('Article Title and what\'s article about are required').should('be.visible');
    });
  });
  